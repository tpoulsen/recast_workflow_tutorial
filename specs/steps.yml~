skimming:
 process:
   process_type: interpolated-script-cmd
   script: |
     # Source the ATLAS environment
     source /release_setup.sh 
     # Run the AnalysisPayload executable to produce the output ROOT file, looping over **all** events.
     source x86_64-centos7-gcc8-opt/setup.sh
     AnalysisPayload {input_file} {output_file}
 environment:
   environment_type: docker-encapsulated
   image: gitlab-registry.cern.ch/tpoulsen/event-selection-tutorial
   imagetag: master-b53cf738
 publisher:
   publisher_type: interpolated-pub
   publish:
     selected_events: '{output_file}'

scaling:
 process:
   process_type: interpolated-script-cmd
   script: |
     source /home/atlas/release_setup.sh
     cd /code
     python scale_to_lumi_xsec.py -i {input_file} \
                                  -o {output_file} \
                                  -p {validation_plot} \
                                  -c {cross_section} \
                                  -s {sum_of_weights} \
                                  -k {k_factor} \
                                  -f {filter_eff} \
                                  -l {luminosity} \
                                  -g {hist}
 environment:
   environment_type: docker-encapsulated
   image: gitlab-registry.cern.ch/recast-atlas/tutorial/post-processing
   imagetag: master-c95d62bf
 publisher:
   publisher_type: interpolated-pub
   publish:
     output_plot: '{validation_plot}'
     output_file: '{output_file}'

fitting:
 process:
   process_type: interpolated-script-cmd
   script: |
     . /recast_auth/getkrb.sh
     xrdcp {filedata} {local_dir}/file_data.root
     xrdcp {filebkg}  {local_dir}/file_bkg.root
     cd /code
     python run_fit.py --filedata {local_dir}/file_data.root --histdata {histdata} --filebkg {local_dir}/file_bkg.root --histbkg {histbkg} --filesig {filesig} --histsig {histsig} --outputfile {outputfile} --plotfile {plotfile}
 environment:
   environment_type: docker-encapsulated
   image: gitlab-registry.cern.ch/recast-atlas/tutorial/fitting
   imagetag: master-02ee489b
   resources:
     - GRIDProxy
 publisher:
   publisher_type: interpolated-pub
   publish:
     output_spectrum: '{plotfile}'
     output_limit:    '{outputfile}'